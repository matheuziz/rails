require 'test_helper'

class GamificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gamification = gamifications(:one)
  end

  test "should get index" do
    get gamifications_url
    assert_response :success
  end

  test "should get new" do
    get new_gamification_url
    assert_response :success
  end

  test "should create gamification" do
    assert_difference('Gamification.count') do
      post gamifications_url, params: { gamification: { negative: @gamification.negative, positive: @gamification.positive, reason: @gamification.reason } }
    end

    assert_redirected_to gamification_url(Gamification.last)
  end

  test "should show gamification" do
    get gamification_url(@gamification)
    assert_response :success
  end

  test "should get edit" do
    get edit_gamification_url(@gamification)
    assert_response :success
  end

  test "should update gamification" do
    patch gamification_url(@gamification), params: { gamification: { negative: @gamification.negative, positive: @gamification.positive, reason: @gamification.reason } }
    assert_redirected_to gamification_url(@gamification)
  end

  test "should destroy gamification" do
    assert_difference('Gamification.count', -1) do
      delete gamification_url(@gamification)
    end

    assert_redirected_to gamifications_url
  end
end
