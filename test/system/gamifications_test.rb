require "application_system_test_case"

class GamificationsTest < ApplicationSystemTestCase
  setup do
    @gamification = gamifications(:one)
  end

  test "visiting the index" do
    visit gamifications_url
    assert_selector "h1", text: "Gamifications"
  end

  test "creating a Gamification" do
    visit gamifications_url
    click_on "New Gamification"

    fill_in "Negative", with: @gamification.negative
    fill_in "Positive", with: @gamification.positive
    fill_in "Reason", with: @gamification.reason
    click_on "Create Gamification"

    assert_text "Gamification was successfully created"
    click_on "Back"
  end

  test "updating a Gamification" do
    visit gamifications_url
    click_on "Edit", match: :first

    fill_in "Negative", with: @gamification.negative
    fill_in "Positive", with: @gamification.positive
    fill_in "Reason", with: @gamification.reason
    click_on "Update Gamification"

    assert_text "Gamification was successfully updated"
    click_on "Back"
  end

  test "destroying a Gamification" do
    visit gamifications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Gamification was successfully destroyed"
  end
end
