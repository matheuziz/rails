json.extract! gamification, :id, :reason, :positive, :negative, :created_at, :updated_at
json.url gamification_url(gamification, format: :json)
