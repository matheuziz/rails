json.extract! affair, :id, :name, :user_id, :created_at, :updated_at
json.url affair_url(affair, format: :json)
