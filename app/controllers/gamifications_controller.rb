class GamificationsController < StaffController
  before_action :set_gamification, only: [:show, :edit, :update, :destroy]

  # POST /gamifications
  # POST /gamifications.json
  def create
    # @gamification = current_user.gamifications.new(gamification_params)
    @gamification = Gamification.new(gamification_params)
    respond_to do |format|
      if @gamification.save
        format.html { redirect_to action:"edit",controller:"users",id:@gamification.user_id, notice: 'Gamification was successfully created.' }
      else
        format.json { render json: @gamification.errors, status: :unprocessable_entity }
      end
    end
  end
  # DELETE /gamifications/1
  # DELETE /gamifications/1.json
  def destroy
    @gamification.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: 'Gamification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gamification
      @gamification = Gamification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gamification_params
      params.require(:gamification).permit(:reason, :positive, :negative,:user_id)
    end
end
