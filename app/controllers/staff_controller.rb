class StaffController < ApplicationController
    before_action :require_admin

def index
end
private
        def require_admin
            #primeiro uso
            if User.first == User.last && User.first == nil
                return
            end
            unless current_user.try(:admin?)
                redirect_to root_path, alert: 'Admins only'
            end
        end
end