class AffairsController < StaffController
  before_action :set_affair, only: [:show, :edit, :update, :destroy]
  before_action :require_admin, except: [:show]
  layout "admin_lte_2"
  # GET /affairs
  # GET /affairs.json
  def index
    @affairs = Affair.all
  end

  # GET /affairs/1
  # GET /affairs/1.json
  def show
  end

  # GET /affairs/new
  def new
    @affair = Affair.new
    @users = User.all
  end

  # GET /affairs/1/edit
  def edit
    @users = User.all
  end

  # POST /affairs
  # POST /affairs.json
  def create
    @affair = Affair.new(affair_params)

    respond_to do |format|
      if @affair.save
        format.html { redirect_to @affair, notice: 'Affair was successfully created.' }
        format.json { render :show, status: :created, location: @affair }
      else
        format.html { render :new }
        format.json { render json: @affair.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /affairs/1
  # PATCH/PUT /affairs/1.json
  def update
    if add?
      user = User.find(params[:affair][:users])
      @affair.users << user
      teste = true
      user.histories.each do |h|
        if h.project_name == @affair.name
          teste = false
        end
      end
      if teste
        history = History.new
        history.project_name = @affair.name
        user.histories << history
      end
      redirect_back fallback_location: root_path, notice: 'User was successfully allocated.'
    else
      respond_to do |format|
        if @affair.update(affair_params)
          format.html { redirect_to @affair, notice: 'Affair was successfully updated.' }
          format.json { render :show, status: :ok, location: @affair }
        else
          format.html { render :edit }
          format.json { render json: @affair.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  def members
    set_affaire
    @users = User.all
  end

  # DELETE /affairs/1
  # DELETE /affairs/1.json
  def destroy
    if del?
      @affair.users.delete(User.find(params[:affair][:users]))
      redirect_back(fallback_location: root_path)
    else
      @affair.destroy
      respond_to do |format|
        format.html { redirect_to affairs_url, notice: 'Affair was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affair
      @affair = Affair.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def affair_params
      params.require(:affair).permit(:name, :users)
    end
    def del?
      params[:commit] == 'Desalocar Membro'
    end
    def add?
      params[:commit] == 'Alocar Membro'
    end
end
