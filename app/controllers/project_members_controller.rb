class ProjectMembersController < StaffController
    layout "admin_lte_2"
    def new
        @affair = Affair.find(params[:affair_id])
        @users = []
        User.all.each do |u|
            unless @affair.users.include? u
                @users << u
            end
        end
    end
    def delete
        @affair = Affair.find(params[:affair_id])
        @users = []
        User.all.each do |u|
            if @affair.users.include? u
                @users << u
            end
        end
    end
    private

end
