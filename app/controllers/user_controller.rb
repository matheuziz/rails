class UserController < ApplicationController
    before_action :authenticate_user!
    

    def index
        redirect_to "/users/#{current_user.id}"
    end

    private

    # def redirect_admin
    #     if current_user.try(:admin?)
    #         redirect_to staff_root_url
    #     end
    # end
end
