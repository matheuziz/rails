class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :rememberable, :validatable
  has_and_belongs_to_many :affairs
  has_many :gamifications
  has_many :histories
  mount_uploader :avatar, AvatarUploader
end
