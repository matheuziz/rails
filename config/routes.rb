Rails.application.routes.draw do
  resources :gamifications, except: [:new,:show,:index,:edit,:update]
  resources :affairs, :path => "projetos"
  get '/projetos/:affair_id/members/new' => 'project_members#new'
  get '/projetos/:affair_id/members/delete' => 'project_members#delete'
  get '/teste' => 'project_members#teste'
  resources :project_members, except: [:new,:index, :show]
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}
  scope "/staff" do
    resources :users, except: [:show]
  end
  get '/users/:id' => 'users#show'
  get 'staff/users/:id/gamification' => 'users#gamification'
  get '/staff' => 'staff#index', as: :staff_root
  root 'user#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
