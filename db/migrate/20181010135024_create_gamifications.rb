class CreateGamifications < ActiveRecord::Migration[5.2]
  def change
    create_table :gamifications do |t|
      t.string :reason
      t.integer :positive, default: 0
      t.integer :negative, default: 0

      t.timestamps
    end
  end
end
