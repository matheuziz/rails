class AddProfileToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :role, :string
    add_column :users, :birthday, :date
    add_column :users, :info, :text
    add_column :users, :name, :string
    # add_reference :users,:affairs, foreign_key: true

    create_join_table :users, :affairs
  end
end
