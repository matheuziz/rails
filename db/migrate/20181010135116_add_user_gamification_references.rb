class AddUserGamificationReferences < ActiveRecord::Migration[5.2]
  def change
    add_reference :gamifications, :user, foreign_key: true
    add_reference :users, :gamifications, foreign_key: true
  end
end
