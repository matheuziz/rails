# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_23_152354) do

  create_table "affairs", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affairs_users", id: false, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "affair_id", null: false
  end

  create_table "gamifications", force: :cascade do |t|
    t.string "reason"
    t.integer "positive", default: 0
    t.integer "negative", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_gamifications_on_user_id"
  end

  create_table "histories", force: :cascade do |t|
    t.string "project_name"
    t.integer "user_id"
    t.index ["user_id"], name: "index_histories_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.date "birthday"
    t.text "info"
    t.string "name"
    t.integer "gamifications_id"
    t.boolean "admin", default: false
    t.string "avatar"
    t.integer "histories_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["gamifications_id"], name: "index_users_on_gamifications_id"
    t.index ["histories_id"], name: "index_users_on_histories_id"
  end

end
